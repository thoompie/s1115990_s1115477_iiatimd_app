package com.ThoompieEnBas.livingdex;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MijnPokedexDAO {

    @Query("SELECT * FROM MijnPokedex")
    List<MijnPokedex> getAll();

    @Query("SELECT COUNT(pokedex_id) FROM MijnPokedex")
    int getRowCount();

    @Query("SELECT COUNT(pokedex_id) FROM MijnPokedex WHERE collected")
    int getCollectedCount();


    @Query("UPDATE MijnPokedex SET collected = :coll WHERE pokedex_id = :pokedexId")
    public void updateCollected( int pokedexId,boolean coll);

    @Insert
    void InsertAll(List<MijnPokedex> mijnPokedex);

    @Query("SELECT MijnPokedex.pokedex_id as pokedexid, MijnPokedex.collected as collected, Pokedex.name as name, Pokedex.description as description from MijnPokedex LEFT JOIN Pokedex ON MijnPokedex.pokedex_id = Pokedex.pokedexNumber ORDER BY MijnPokedex.pokedex_id")
    public List<PokeList> getJoinedAll();
}
