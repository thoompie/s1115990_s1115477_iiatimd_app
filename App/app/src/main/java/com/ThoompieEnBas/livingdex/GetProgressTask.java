package com.ThoompieEnBas.livingdex;

import android.util.Log;
import android.view.View;

public class GetProgressTask implements Runnable{
    private PokedexDatabase db;

    GetProgressTask(PokedexDatabase db) {
        this.db = db;
    }

    @Override
    public void run() {
        try{
            if(db.mijnPokedexDAO().getRowCount() != 0) {
                MainActivity.collectedProgressBar.setVisibility(View.VISIBLE);
                int max = db.mijnPokedexDAO().getRowCount();
                MainActivity.collectedProgressBar.setMax(max);
                int progres = db.mijnPokedexDAO().getCollectedCount();
                MainActivity.collectedProgressBar.setProgress(progres);
                double percent = ((double)progres / (double) max) * 100;
                MainActivity.collectedTextView.setVisibility(View.VISIBLE);
                MainActivity.percentTextView.setText(Math.round(percent) + "%");
                MainActivity.percentTextView.setVisibility(View.VISIBLE);
            }
        }catch(Exception e){
            Log.d("hoi", "Database is leeg");
        }
    }
}
