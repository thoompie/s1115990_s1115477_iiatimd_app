package com.ThoompieEnBas.livingdex;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.SearchView;

import java.util.HashMap;

public class PokelistActivity extends AppCompatActivity implements PokemonAdapter.onItemClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private PokemonAdapter adapter;

    private Context context = this;

    public static PokedexDatabase pokedexDatabase;

    private static void createDatabase(Context context){
        pokedexDatabase = PokedexDatabase.getInstance(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        createDatabase(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokelist);


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.hasFixedSize();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new PokemonAdapter(MainActivity.mijnPokedex, this);
       // recyclerViewAdapter = new PokemonAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onButtonInfoClick(int position, Drawable plaatje) {

        Intent intent = new Intent(this, PokemonInfoActivity.class);
        intent.putExtra("position", ""+ position);


        startActivity(intent);
    }

    @Override
    //Maakt de menu kleiner en pakt plaats de zoek bar
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pokemon_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView =  (SearchView) searchItem.getActionView();

        //Als de text veranderd voer de filter functie uit
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
