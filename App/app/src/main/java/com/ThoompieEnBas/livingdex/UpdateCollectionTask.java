package com.ThoompieEnBas.livingdex;

import android.os.AsyncTask;
import android.util.Log;

import com.ThoompieEnBas.livingdex.PokedexDatabase;

import java.lang.ref.WeakReference;

public class UpdateCollectionTask  implements Runnable{

    private PokedexDatabase db;
    private int pokedexId;
    private boolean coll;

    UpdateCollectionTask(PokedexDatabase db, int pokedexId, boolean coll) {
        this.db = db;
        this.pokedexId = pokedexId;
        this.coll = coll;
    }

    @Override
    public void run() {
        try{
            db.mijnPokedexDAO().updateCollected(pokedexId,coll);
        }catch(Exception e){
            Log.d("error", "update lukt niet");
        }
    }
}
