package com.ThoompieEnBas.livingdex;

import android.util.Log;

import java.util.List;

public class InsertCollectionTask implements Runnable{

    private PokedexDatabase db;
    private List<Pokedex> insertArray;
    private List<MijnPokedex> mijnPokedexList;


    InsertCollectionTask(PokedexDatabase db, List<Pokedex> insertArray, List<MijnPokedex> mijnPokedexList) {
        this.db = db;
        this.mijnPokedexList = mijnPokedexList;
        this.insertArray = insertArray;
    }

    @Override
    public void run() {
        try {
           db.pokedexDAO().InsertAll(insertArray);
           db.mijnPokedexDAO().InsertAll(mijnPokedexList);
           UpdateDatabaseActivity.insertCollectionDone = true;
           UpdateDatabaseActivity.progress.incrementProgressBy(50);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
