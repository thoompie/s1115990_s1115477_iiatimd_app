package com.ThoompieEnBas.livingdex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UpdateDatabaseActivity extends AppCompatActivity {
    public PokedexDatabase pokedexDatabase;
    public static boolean insertCollectionDone = false;
    public static boolean insertLocationDone = false;
    public static ProgressBar progress;
    private Button doneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_database);
        doneButton = findViewById(R.id.doneButton);
        pokedexDatabase = PokedexDatabase.getInstance(this);
        progress = findViewById(R.id.progressBar);

        final String URL = "https://pokemon-api-project.herokuapp.com/api/pokedex";
        final String URLlocation = "https://pokemon-api-project.herokuapp.com/api/locations";
        final String keyPokedex = "-MBnxFknFK7zAd-Gw9BH";
        final String keyLocation = "-MBnxq6uSuIBzi6saW37";

        RequestQueue queue = VolleySingleton.getinstance(this.getApplicationContext()).getRequestQueue();
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent myIntent = new Intent(v.getContext(), MainActivity.class);
              startActivityForResult(myIntent, 0);
            }
        });
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arr = response.getJSONArray(keyPokedex);
                    List<Pokedex> insertArray = new ArrayList<Pokedex>();
                    List<MijnPokedex> mijnPokedexList = new ArrayList<MijnPokedex>();
                    for (int i = 0; i < arr.length(); i++) {
                        try {
                            insertArray.add(new Pokedex(
                                    arr.getJSONObject(i).getString("name"),
                                    arr.getJSONObject(i).getString("description"),
                                    arr.getJSONObject(i).getInt("pokedexNumber")));
                            mijnPokedexList.add(new MijnPokedex(
                                    arr.getJSONObject(i).getInt("pokedexNumber"),
                                    false,
                                    8
                            ));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Log.d("hoi", "" + mijnPokedexList.get(7).getPokedex_id());
                    new Thread(new InsertCollectionTask(pokedexDatabase, insertArray, mijnPokedexList)).start();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("niet gelukt", error.getMessage());
            }
        });

        queue.add(jsonObjectRequest);


        JsonObjectRequest jsonObjectRequestLocation = new JsonObjectRequest(Request.Method.GET, URLlocation, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrLocation = response.getJSONArray(keyLocation);
                    List<Locations> LocationArray = new ArrayList<Locations>();
                    for (int i = 0; i < arrLocation.length(); i++) {
                        try {
                            LocationArray.add(new Locations(
                                    arrLocation.getJSONObject(i).getString("location"),
                                    arrLocation.getJSONObject(i).getInt("generation"),
                                    arrLocation.getJSONObject(i).getInt("pokedex_id")));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    new Thread(new InsertLocationTask(pokedexDatabase, LocationArray)).start();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("niet gelukt", error.getMessage());
            }
        });

        queue.add(jsonObjectRequestLocation);


        while(insertCollectionDone = false && insertLocationDone == false){

        }

        doneButton.setVisibility(View.VISIBLE);
        progress.setVisibility(View.INVISIBLE);
    }
}