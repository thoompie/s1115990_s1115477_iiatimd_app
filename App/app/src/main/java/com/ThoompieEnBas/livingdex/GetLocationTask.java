package com.ThoompieEnBas.livingdex;

import android.util.Log;

public class GetLocationTask implements Runnable{

    private PokedexDatabase db;
    private int pokedex_id;

    GetLocationTask(PokedexDatabase db, int pokedex_id) {
        this.db = db;
        this.pokedex_id = pokedex_id;
    }

    @Override
    public void run() {
        try{
            String locations = db.locationsDAO().getLocation(pokedex_id);
            String[] locsplits = locations.split(", ");
            String pokemonlocations = "";
            for(int i = 0; i < locsplits.length; i++){
                pokemonlocations += (locsplits[i] +"\n");
               // Log.d("hoi", "hey " + locsplits[i]);
               // Log.d("hoi", "hey " + pokemonlocations);
            }

          //  PokemonInfoActivity.locations = db.locationsDAO().getLocation(1);
            //PokemonInfoActivity.textView_location.setText( db.locationsDAO().getLocation(pokedex_id));
           PokemonInfoActivity.textView_location.setText(pokemonlocations);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
