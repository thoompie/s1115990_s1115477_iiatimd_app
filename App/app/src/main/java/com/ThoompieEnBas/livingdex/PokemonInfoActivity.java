package com.ThoompieEnBas.livingdex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import static java.lang.Integer.parseInt;

public class PokemonInfoActivity extends AppCompatActivity {
    TextView textView_nummer;
    TextView textView_naam;
    static TextView textView_location;
    TextView textView_description;
    public static String locations;
    public PokedexDatabase pokedexDatabase;
    ImageView imageView;
    ImageView terugImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pokedexDatabase = PokedexDatabase.getInstance(this.getApplicationContext());
        setContentView(R.layout.activity_pokemon_info);

        textView_nummer = findViewById(R.id.textView_nummer);
        textView_naam = findViewById(R.id.textView_naam);
        textView_location = findViewById(R.id.textView_location);
        textView_description = findViewById(R.id.textView_info);
        imageView = findViewById(R.id.img_pokemon_info);
        terugImage = findViewById(R.id.info_back_image_button);



        //pakt de items uit de mee gegeven bundle
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        String pos = bundle.getString("position");
        new Thread(new GetLocationTask(pokedexDatabase, MainActivity.mijnPokedex.get(parseInt(pos)-1).getPokedexid())).start();


        textView_nummer.setText("#"+ MainActivity.mijnPokedex.get(parseInt(pos)-1).getPokedexid());
        textView_naam.setText( MainActivity.mijnPokedex.get(parseInt(pos)-1).getName());
        textView_description.setText( MainActivity.mijnPokedex.get(parseInt(pos)-1).getDescription());


        //Plaatje pakken en hem in het scherm zetten
        Drawable plaatje = this.getResources().getDrawable(
                this.getResources().getIdentifier(
                        "p_" + (MainActivity.mijnPokedex.get(parseInt(pos)-1).getPokedexid()), "drawable", this.getPackageName()
                ));
        imageView.setImageDrawable(plaatje);

        terugImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
