package com.ThoompieEnBas.livingdex;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MijnPokedex {

    @ColumnInfo
    private int pokedex_id;

    @ColumnInfo
    private Boolean collected;

    @ColumnInfo
    private int currentGeneration;

    @PrimaryKey(autoGenerate = true)
    private int id;

    public MijnPokedex (int pokedex_id, boolean collected, int currentGeneration) {
        this.pokedex_id = pokedex_id;
        this.collected = collected;
        this.currentGeneration = currentGeneration;
    }

    public int getPokedex_id() {
        return pokedex_id;
    }

    public Boolean getCollected() {
        return collected;
    }

    public void setCollected(boolean collected){this.collected = collected;}

    public int getCurrentGeneration() {
        return currentGeneration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
