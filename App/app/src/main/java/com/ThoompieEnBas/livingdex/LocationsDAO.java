package com.ThoompieEnBas.livingdex;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LocationsDAO {

    @Query("SELECT * FROM locations")
    List<Locations> getAll();

    @Query("SELECT location from locations WHERE pokedex_id = :id")
    String getLocation(int id);

    @Insert
    void InsertAll(List<Locations> locations);
}
