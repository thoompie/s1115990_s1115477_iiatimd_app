package com.ThoompieEnBas.livingdex;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PokedexDAO {

    @Query("SELECT * FROM Pokedex")
    public List<Pokedex> getAll();

    @Query("INSERT INTO Pokedex(name, description, pokedexNumber) VALUES(:name,:description, :pokedexNumber)")
    void InsertPokedex(String name, String description, int pokedexNumber);

    @Insert
    void InsertAll(List<Pokedex> pokedex);
}
