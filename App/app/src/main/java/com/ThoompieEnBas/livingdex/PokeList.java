package com.ThoompieEnBas.livingdex;

public class PokeList {

    private int pokedexid;
    private String name;
    private String description;
    private boolean collected;

    public PokeList(int pokedexid, String name, String description, boolean collected) {
        this.pokedexid = pokedexid;
        this.name = name;
        this.description = description;
        this.collected = collected;
    }

    public int getPokedexid() {
        return pokedexid;
    }

    public void setPokedexid(int pokedexid) {
        this.pokedexid = pokedexid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getCollected() {
        return collected;
    }

    public void setCollected(boolean collected) {
        this.collected = collected;
    }
}
