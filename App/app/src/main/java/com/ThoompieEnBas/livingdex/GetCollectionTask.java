package com.ThoompieEnBas.livingdex;

import android.util.Log;

import java.util.List;

public class GetCollectionTask implements Runnable{

    private PokedexDatabase db;

    GetCollectionTask(PokedexDatabase db) {
        this.db = db;
    }

    @Override
    public void run() {
        try{
            if(db.mijnPokedexDAO().getRowCount() != 0) {
                MainActivity.mijnPokedex = db.mijnPokedexDAO().getJoinedAll();
            }
        }catch(Exception e){
            Log.d("hoi", "Database is leeg");
        }
    }
}
