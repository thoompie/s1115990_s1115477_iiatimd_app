package com.ThoompieEnBas.livingdex;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder> implements Filterable {
    private List<PokeList> pokemon;
    private List<PokeList> pokemonListFulll;
    private onItemClickListener mOnItemClickListener;


    public PokemonAdapter(List<PokeList> pokemon, onItemClickListener onItemClickListener){
        this.pokemon = pokemon;
        pokemonListFulll = new ArrayList<>(pokemon);
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    //Filteren van de pokemon kaarten op search
    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PokeList> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(pokemonListFulll);
                PokemonViewHolder.Searched = false;
            } else{
                PokemonViewHolder.Searched = true;
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PokeList pokemon : pokemonListFulll){
                    if(pokemon.getName().toLowerCase().contains(filterPattern) || Integer.toString(pokemon.getPokedexid()).contains(filterPattern)){
                        filteredList.add(pokemon);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        //geeft de resultaten mee
        protected void publishResults(CharSequence constraint, FilterResults results) {
            pokemon.clear();
            pokemon.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public static class PokemonViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        public ImageView myImage;
        public Context context;

        public onItemClickListener onItemClickListener;

        public CardView card;
        public ImageButton button;
        public boolean isClicked;

        public static boolean Searched = false;

        public PokemonViewHolder(View v, final onItemClickListener onItemClickListener){
            super(v);
            textView = v.findViewById(R.id.textView);
            myImage = v.findViewById(R.id.imageView);
            card = (CardView) v.findViewById(R.id.cardView);
            context  = v.getContext();

            button = v.findViewById(R.id.imageButton);
            isClicked = false;

            this.onItemClickListener = onItemClickListener;



        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = getAdapterPosition();
                Drawable plaatje = v.getContext().getResources().getDrawable(
                        v.getContext().getResources().getIdentifier(
                                "p_" + (position+1), "drawable", v.getContext().getPackageName()
                        ));

                Log.d( "hey", "hey: " + position);
                onItemClickListener.onButtonInfoClick(position + 1, plaatje);
            }
        });

        }
    }

    //override voor box nummers
    @Override
    public int getItemViewType(int position){
        int realValue = position+1;
        if (realValue == 1 || position%30 == 0)
            return realValue; //welk nummer het is
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if(viewType != 0 && !PokemonViewHolder.Searched){
            v = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.pokemon_card_boxheader,parent,false);
            TextView boxtext = v.findViewById(R.id.textViewBox);
            int viewTypeMax = viewType + 29;
            boxtext.setText("#" + viewType + " - " + "#" + viewTypeMax);
        }else{
             v = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.pokemon_card,parent,false);
        }

        PokemonViewHolder pokemonViewHolder = new PokemonViewHolder(v, mOnItemClickListener);
        return pokemonViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final PokemonViewHolder holder, final int position) {
        holder.textView.setText("#" + ( pokemon.get(position).getPokedexid()) + " " + pokemon.get(position).getName());
        holder.isClicked = pokemon.get(position).getCollected();
        if(holder.isClicked == true){
            holder.button.setBackgroundResource(R.drawable.checkmark);
            holder.card.setCardBackgroundColor(holder.context.getResources().getColor(R.color.collected));
        }else{
            holder.button.setBackgroundResource(R.drawable.circle);
            holder.card.setCardBackgroundColor(holder.context.getResources().getColor(R.color.uncollected));
        }

        holder.button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(holder.isClicked == false) {
                    holder.isClicked = true;
                    holder.button.setBackgroundResource(R.drawable.checkmark);
                    holder.card.setCardBackgroundColor(holder.context.getResources().getColor(R.color.collected));
                    pokemon.get(position).setCollected(true);
                    //stop de pokemon in jou database
                    new Thread(new UpdateCollectionTask(PokelistActivity.pokedexDatabase,( pokemon.get(position).getPokedexid()) ,true)).start();
                }else{
                    holder.isClicked = false;
                    holder.button.setBackgroundResource(R.drawable.circle);
                    holder.card.setCardBackgroundColor(holder.context.getResources().getColor(R.color.uncollected));
                    pokemon.get(position).setCollected(false);
                    //haal uit de database
                    new Thread(new UpdateCollectionTask(PokelistActivity.pokedexDatabase,( pokemon.get(position).getPokedexid()) ,false)).start();
                }
            }
        });
        try{
            Drawable draw = holder.context.getResources().getDrawable(
                    holder.context.getResources().getIdentifier(
                            "p_" + ( pokemon.get(position).getPokedexid()), "drawable", holder.context.getPackageName()
                    ));
            holder.myImage.setImageDrawable(draw);
            //holder.card.setBackgroundResource(R.color.collected);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface onItemClickListener{
        void onButtonInfoClick(int position, Drawable plaatje);
    }

    @Override
    public int getItemCount() {
        return pokemon.size();
    }
}
