package com.ThoompieEnBas.livingdex;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Locations {
    @ColumnInfo
    private int pokedex_id;

    @ColumnInfo
    private int generation;

    @ColumnInfo
    private String location;

    @PrimaryKey(autoGenerate = true)
    private int id;


    public Locations (String location, int generation, int pokedex_id) {
        this.location = location;
        this.generation = generation;
        this.pokedex_id = pokedex_id;
    }

    public String getLocation(){
        return location;
    }

    public int getGeneration(){
        return generation;
    }

    public int getPokedex_id() {
        return pokedex_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
