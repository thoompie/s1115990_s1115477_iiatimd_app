package com.ThoompieEnBas.livingdex;

import android.util.Log;

import java.util.List;

public class InsertLocationTask implements Runnable{

    private PokedexDatabase db;
    private List<Locations> locations;



    InsertLocationTask(PokedexDatabase db, List<Locations> locations) {
        this.db = db;
        this.locations = locations;

    }

    @Override
    public void run() {

        try {
            db.locationsDAO().InsertAll(locations);
            UpdateDatabaseActivity.insertLocationDone = true;
            UpdateDatabaseActivity.progress.incrementProgressBy(50);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
