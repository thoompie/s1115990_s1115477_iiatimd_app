package com.ThoompieEnBas.livingdex;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Entity(foreignKeys = {@ForeignKey(entity = Pokedex.class,
        parentColumns = "id",
        childColumns = "pokedex_id",
        onDelete = ForeignKey.CASCADE),
         @ForeignKey(entity = Pokedex.class,
                parentColumns = "id",
                childColumns = "pokedex_id",
                onDelete = ForeignKey.CASCADE)})


@Database(entities = {Pokedex.class, Locations.class, MijnPokedex.class}, version = 20, exportSchema = false)
public abstract class PokedexDatabase extends RoomDatabase {
    public abstract PokedexDAO pokedexDAO();
    public abstract MijnPokedexDAO mijnPokedexDAO();
    public abstract LocationsDAO locationsDAO();

    private static PokedexDatabase instance;

    static synchronized PokedexDatabase getInstance(Context context){
        if(instance == null){
            instance = create(context);
        }
        return instance;
    }

    private static PokedexDatabase create(final Context context){
        return Room.databaseBuilder(context,PokedexDatabase.class,"pokedex").fallbackToDestructiveMigration().build();
    }

}
