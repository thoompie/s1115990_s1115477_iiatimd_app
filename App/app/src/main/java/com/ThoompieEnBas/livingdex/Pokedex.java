package com.ThoompieEnBas.livingdex;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Pokedex {
    @ColumnInfo
    private String name;
    @ColumnInfo
    private String description;

    @ColumnInfo
    private int pokedexNumber;

    @PrimaryKey(autoGenerate = true)
    private int id;

    public Pokedex (String name, String description, int pokedexNumber) {
        this.name = name;
        this.description = description;
        this.pokedexNumber = pokedexNumber;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public int getPokedexNumber(){
        return pokedexNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
