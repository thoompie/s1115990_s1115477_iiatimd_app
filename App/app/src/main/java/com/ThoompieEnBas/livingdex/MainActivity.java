package com.ThoompieEnBas.livingdex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static int numberOfPokemon = 890;
    public PokedexDatabase pokedexDatabase;
    public static List<PokeList> mijnPokedex;
    public static ProgressBar collectedProgressBar;
    public static TextView collectedTextView;
    public static TextView percentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new Thread(new GetCollectionTask(pokedexDatabase)).start();
        pokedexDatabase = PokedexDatabase.getInstance(this.getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        collectedProgressBar = findViewById(R.id.progressbar_pokemons_collected);
        collectedTextView = findViewById(R.id.textView_progress);
        percentTextView = findViewById(R.id.textView_percent);
        new Thread(new GetProgressTask(pokedexDatabase)).start();

        Button startbutton = (Button) findViewById(R.id.Startbutton);
        startbutton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                if(mijnPokedex != null){
                    Intent myIntent = new Intent(view.getContext(), PokelistActivity.class);
                    startActivityForResult(myIntent, 0);
                }else{
                    Intent myIntent = new Intent(view.getContext(), UpdateDatabaseActivity.class);
                    startActivityForResult(myIntent, 0);
                }

            }
        });


    }

    @Override
    protected void onResume(){
        super.onResume();
        new Thread(new GetCollectionTask(pokedexDatabase)).start();
        new Thread(new GetProgressTask(pokedexDatabase)).start();

        ImageView image = (ImageView) findViewById(R.id.imageHomePage);
        int randomNumber = (int) Math.floor(Math.random() * Math.floor(numberOfPokemon));

        Drawable draw = this.getResources().getDrawable(
                this.getResources().getIdentifier(
                        "p_" + (randomNumber+1), "drawable", this.getPackageName()
                ));
        image.setImageDrawable(draw);
    }


}